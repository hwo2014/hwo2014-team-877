#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>

#include "messaging.h"
#include "structs.h"

//COMMENCE HARDCODED BULLSHIT
const double time_scale = 50.0/539.0;
const double a = 1.145;
const double b = 1.03;
const double c = 1100;
const double threshold = 0.27777777777;

char *bot_name;

static double sign(double x)
{
	if (x < 0) return -1.0;
	return 1.0;
}

typedef struct ODEParams {
	double vel;
	double radius;
	double sign;
} ODEParams;

Track* parse_track(cJSON *json)
{
	int i;
	cJSON *track = cJSON_GetObjectItem(
		cJSON_GetObjectItem(
		cJSON_GetObjectItem(json, "data"), "race"), "track");
	cJSON *pieces = cJSON_GetObjectItem(track, "pieces");
	cJSON *lanes = cJSON_GetObjectItem(track, "lanes");

	//getpieces
	size_t num_pieces = cJSON_GetArraySize(pieces);
	TrackPiece *p = (TrackPiece*)malloc(sizeof(TrackPiece)*num_pieces);
	for(i=0;i<num_pieces;i++) {
		cJSON *piece = cJSON_GetArrayItem(pieces, i);
		cJSON *length = cJSON_GetObjectItem(piece, "length");
		cJSON *radius = cJSON_GetObjectItem(piece, "radius");
		cJSON *angle = cJSON_GetObjectItem(piece, "angle");
		cJSON *switch_piece = cJSON_GetObjectItem(piece, "switch");

		if(length != NULL) (p+i)->length = length->valuedouble;
		else (p+i)->length = -1;
		if(radius != NULL) (p+i)->radius = radius->valuedouble;
		else (p+i)->radius = 0.0;
		if(angle != NULL) (p+i)->angle = angle->valuedouble;
		else (p+i)->angle = 0.0;
		if(switch_piece != NULL) (p+i)->is_switch_piece = 1;
		else (p+i)->is_switch_piece = 0;
	}

	//getlanes
	size_t num_lanes = cJSON_GetArraySize(lanes);
	double *l = (double*)malloc(sizeof(double)*num_lanes);
	for(i=0;i<num_lanes;i++) {
		cJSON *lane = cJSON_GetArrayItem(lanes, i);
		*(l+i) = cJSON_GetObjectItem(lane, "distanceFromCenter")->valuedouble;
	}

	Track *t = (Track*)malloc(sizeof(Track));
	t->track_pieces = p; t->lanes = l; t->num_pieces = num_pieces;
	t->num_lanes = num_lanes;
	return t;
}

void get_own_state(CarState *cs, cJSON *json)
{
	int i;
	cJSON *data = cJSON_GetObjectItem(json, "data");
	for(i=0;i<cJSON_GetArraySize(data);i++) {
		cJSON *subitem = cJSON_GetArrayItem(data,i);
		if(!strcmp(cJSON_GetObjectItem(cJSON_GetObjectItem(subitem,"id"), "name")->valuestring, bot_name)) {
			cs->angle = cJSON_GetObjectItem(subitem, "angle")->valuedouble;
			cs->piece_distance = cJSON_GetObjectItem(
			cJSON_GetObjectItem(subitem, "piecePosition"), "inPieceDistance")->valuedouble;
			cs->piece_index = cJSON_GetObjectItem(
			cJSON_GetObjectItem(subitem, "piecePosition"), "pieceIndex")->valueint; 
			cs->current_lane = cJSON_GetObjectItem(cJSON_GetObjectItem(
			cJSON_GetObjectItem(subitem, "piecePosition"), "lane"), "startLaneIndex")->valueint;
			break;
		}
	}
}

double get_radius(int piece, int lane, Track *track)
{
	TrackPiece* cur_piece = track->track_pieces + piece;
	if(cur_piece->length != -1) return 0.0;
	double cur_offset = *(track->lanes + lane);
	double radius;
	if(cur_piece->angle > 0) {
		radius = cur_piece->radius - cur_offset; 
	} else {
		radius = cur_piece->radius + cur_offset;
	}
	return radius;
}

int func(double t, const double y[], double f[],
		void *params)
{
	ODEParams prms = *(ODEParams*)params;
	f[0] = y[1];
	if(prms->radius == 0.0 || pow(prms->vel,2.0)/prms->radius < threshold) f[1] = -a * y[0] - b * y[1];
	else f[1] = prms->sign * c * pow(pow(prms->vel,2.0)/prms->radius, 4.0) - a * y[0] - b * y[1];
	return GSL_SUCCESS;
}

static int will_crash(CarState *now, CarState *last, Track *track, double throttle)
{
	int i;
	double vel = get_speed(now, last, track);
	double start_vel = vel;
	double cur_pos = now->piece_distance;
	int cur_piece = now->piece_index;
	int cur_lane = now->lane_index;
	double radius = get_radius(cur_piece, cur_lane, track);
	double sign = sign((track->track_pieces + cur_piece)->angle);

	ODEParams params = { vel, radius, sign };
	gsl_odeiv2_system sys = { func, NULL, 2, &params };
	gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(
		&sys, gsl_odeiv2_step_rk8pd,
		1e-6, 1e-6, 0.0);

	double t0 = 0.0, t = 0.0;
	for (i = 0; i<10000; i++) {

		t += (double)i / 100.0;
		gsl_odeiv2_driver_apply(d, &t0, t*time_scale, y);

		if (y[0] > 59.9 || y[0] < -59.9) {
			gsl_odeiv2_driver_free(d);
			return 1;
		}

		if (i > 100) vel *= pow(0.98,t - 1.0);
		else vel += 0.2*(throttle - start_vel / 10.0) * t;
		cur_pos += vel*t;
		if (cur_pos>get_piece_length(cur_piece, cur_lane, track)) {
			cur_pos = 0.0;
			cur_piece += 1;
			if (cur_piece == track->num_pieces) cur_piece = 0;
			cur_lane = (track->track_pieces + params->cur_piece)->ideal_lane_index;
		}
		params->vel = vel;
		params->radius = get_radius(cur_piece, cur_lane, track);
		params->sign = sign((track->track_pieces + cur_piece)->angle);
	}
	gsl_odeiv2_driver_free(d);
	return 0;
}

static cJSON *handle_throttle(CarState *now, CarState *last, Track *track)
{
	TrackPiece* cur_piece = (track->track_pieces + now->piece_index);
	TrackPiece* next_piece;
	if(now->piece_index + 1 == track->num_pieces) next_piece = track->track_pieces;
	else next_piece = (track->track_pieces + now->piece_index + 1);

	// Set throttle
	if(will_crash(now,last,track,1.0) != 1) throttle = 1.0;

	double min = 0.0;
	double max = 1.0;
	double mid;
	int i;
	for (i = 0; i < 20; i++) {
		mid = min + (max - min) / 2.0;
		if (will_crash(now, last, track, mid) != 1) min = mid;
		else max = mid;
	}
	if (will_crash(now, last, track, mid) != 1) throttle = mid;
	else throttle = 0.0;

	// Turbo?
	if(last->throttle == throttle && 
		now->piece_index == track->boost_piece &&
		now->turbo_available == 1) {
		now->turbo_available = 0;
		return turbo_msg();
	}

	// Switch?
	if(last->throttle == throttle && next_piece->is_switch_piece) {
		int cur_lane = now->current_lane;
		int ideal_lane = cur_piece->ideal_lane_index;
		return switch_msg( cur_lane > ideal_lane ? 0 : 1);
	}

	now->throttle = throttle;
	return throttle_msg(throttle);
}

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");


    sock = connect_to(argv[1], argv[2]);
	bot_name = argv[3];
    json = join_race(argv[3], argv[4], "keimola");
    write_msg(sock, json);
    cJSON_Delete(json);

	CarState *state_last = (CarState*)malloc(sizeof(CarState));
	CarState *state = (CarState*)malloc(sizeof(CarState));
	state->turbo_available = 0;

	Track *track;

    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name)) {
			get_own_state(state, json);
			msg = handle_throttle(state, state_last, track);
			memcpy(state_last, state, sizeof(CarState));
		} else if(!strcmp("gameInit", msg_type_name)) {
			track = parse_track(json);
			set_switch_instructions(track);
			set_boost_piece(track);
			msg = ping_msg();
		} else if(!strcmp("turboAvailable",msg_type_name)) {
			state->turbo_available = 1;
			msg = ping_msg();
        } else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

	free(state); free(state_last); 
	free(track->track_pieces); free(track->lanes);
	free(track);

    return 0;
}
