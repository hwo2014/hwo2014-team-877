#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "structs.h"

#define PI 3.14159265

static int sign(double x)
{
    return (x > 0) - (x < 0);
}

double get_piece_length(int piece_index, int lane_index, Track *track)
{
	if((track->track_pieces + piece_index)->length != -1) {
		return (track->track_pieces + piece_index)->length;
	} else {
		double offset = *(track->lanes + lane_index);
		double angle = (track->track_pieces + piece_index)->angle;
		double radius = (track->track_pieces + piece_index)->radius;
        if(offset == 0) {
            return 2.0 * PI * radius * ( abs(angle) / 360.0 );
        } else if(sign(offset) == sign(angle)) {
            return 2.0 * PI * (radius - abs(offset)) * ( abs(angle) / 360.0 );
        } else {
            return 2.0 * PI * (radius + abs(offset)) * ( abs(angle) / 360.0 );
        }

	}
}

int check_switch(int piece_index, Track *track)
{
	return (track->track_pieces + piece_index)->is_switch_piece;
}

double get_speed(CarState *now, CarState *last, Track *track)
{
	int now_piece = now->piece_index;
	int last_piece = last->piece_index;
	double speed;
	if(now_piece == last_piece) {
		speed = now->piece_distance - last->piece_distance;
	}
	else {
		speed = get_piece_length(last_piece, last->current_lane, track)
				- last->piece_distance
				+ now->piece_distance;
	}
	return speed;
}

void set_switch_instructions(Track *track)
{
	int i;
	int last_ideal_index = -1;
	for(i=0; i<track->num_pieces; i++) {
		if(check_switch(i,track)) {
			double *lane_lengths = (double*)calloc(track->num_lanes, sizeof(double));
			int next = i;
			while(next == i || check_switch(next,track) != 1) {
				int j;
				for(j=0;j<track->num_lanes;j++) {
					*(lane_lengths + j) += get_piece_length(next,j,track);
				}
				next += 1;
				if(next == track->num_pieces) next = 0;
			}
			int ideal_lane_index = 0;
			int j;
			for(j=0;j<track->num_lanes;j++) {
				if(*(lane_lengths+j) < *(lane_lengths+ideal_lane_index))
					ideal_lane_index = j;
				if(*(lane_lengths+j) == *(lane_lengths+ideal_lane_index) &&
					last_ideal_index != -1)
					ideal_lane_index = last_ideal_index;
				//printf("laneindex %d length: %f\n", j, *(lane_lengths+j));
			}
			j=i;
			while(j==i || check_switch(j,track) != 1) {
				//printf("piece %d ideal lane index: %d\n",j,ideal_lane_index);
				(track->track_pieces + j)->ideal_lane_index = ideal_lane_index;
				j += 1;
				if(j == track->num_pieces) j = 0;
			}
			last_ideal_index = ideal_lane_index;
			free(lane_lengths);
		}
	}
}

void set_boost_piece(Track *track)
{
	int boost_piece = 0;
	double boost_length = 0.0;
	int i;
	for(i=0;i<track->num_pieces;i++){
		double length = 0.0;
		int next = i;
		while((track->track_pieces + next)->length != -1) {
			length += (track->track_pieces + next)->length;
			next += 1;
			if(next == track->num_pieces) next = 0;
		}
		if(length > boost_length){
			boost_length = length;
			boost_piece = i;
		}
	}
	track->boost_piece = boost_piece;
}
