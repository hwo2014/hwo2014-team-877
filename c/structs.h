#ifndef STRUCTS_H
#define STRUCTS_H

typedef struct TrackPiece {
	double length;
	double radius;
	double angle;
	double max_vel;
	int ideal_lane_index;
	int is_switch_piece;
} TrackPiece;

typedef struct Track {
	TrackPiece *track_pieces;
	double *lanes;
	size_t num_pieces;
	size_t num_lanes;
	int boost_piece;
} Track;

typedef struct CarState {
	double angle;
	double piece_distance;
	double throttle;
	int current_lane;
	int piece_index;
	int turbo_available;
} CarState;

double get_piece_length(int piece_index, int lane_index, Track *track);
int check_switch(int piece_index, Track *track);

double get_speed(CarState *now, CarState *last, Track *track);

void set_switch_instructions(Track *track);
void set_boost_piece(Track *track);

#endif // STRUCTS_H
