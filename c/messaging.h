#ifndef MESSAGING_H
#define MESSAGING_H

#include "cJSON.h"
#include "structs.h"

void error(char *fmt, ...);
int connect_to(char *hostname, char *port);
void log_message(char *msg_type_name, cJSON *msg);


cJSON *ping_msg();
cJSON *join_msg(char *bot_name, char *bot_key);
cJSON *join_race(char *bot_name, char *bot_key, char *track_name);
cJSON *throttle_msg(double throttle);
cJSON *switch_msg(int direction);
cJSON *turbo_msg();
cJSON *make_msg(char *type, cJSON *msg);

cJSON *read_msg(int fd);
void write_msg(int fd, cJSON *msg);

#endif // MESSAGING_H
